﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Floxdc.MingApi.Middleware
{
    public interface IAuthenticationService
    {
        Task ValidateApiRequestConditions(IHeaderDictionary headers);
    }
}
