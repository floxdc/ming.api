﻿using Microsoft.AspNetCore.Builder;

namespace Floxdc.MingApi.Middleware
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthenticationHandler(this IApplicationBuilder app) 
            => app.UseMiddleware<AuthenticationHandler>();
    }
}
