﻿using System;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Floxdc.MingApi.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace Floxdc.MingApi.Middleware
{
    public class AuthenticationService : IAuthenticationService
    {
        public AuthenticationService(IOptions<AuthenticationOptions> options)
        {
            _options = options;
        }


        public Task ValidateApiRequestConditions(IHeaderDictionary headers)
        {
            var acceptHeaders = GetHeaderValues(headers, "Accept");
            if (!IsAcceptConditionsValid(acceptHeaders))
                throw new NotSupportedException();

            var authorizationHeaders = GetHeaderValues(headers, "Authorization");
            if (!IsAuthorizationConditionsValid(authorizationHeaders))
                throw new InvalidCredentialException();

            return Task.FromResult(0);
        }


        private StringValues GetHeaderValues(IHeaderDictionary headers, string headerName)
        {
            StringValues result;
            headers.TryGetValue(headerName, out result);

            return result;
        }


        private bool IsAcceptConditionsValid(StringValues acceptHeaders)
            => acceptHeaders.Any(header => _options.Value.AcceptableHeaders.Contains(header.ToLower()));


        //TODO
        private bool IsAuthorizationConditionsValid(StringValues authorizationHeaders)
        {
            return true;
        }


        private readonly IOptions<AuthenticationOptions> _options;
    }
}
