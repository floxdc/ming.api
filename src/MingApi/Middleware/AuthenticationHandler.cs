﻿using System;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;
using Floxdc.MingApi.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Floxdc.MingApi.Middleware
{
    public class AuthenticationHandler
    {
        public AuthenticationHandler(RequestDelegate next, IOptions<AuthenticationOptions> options, ILogger<AuthenticationHandler> logger)
        {
            _logger = logger;
            _next = next;
            _options = options;
        }


        public async Task Invoke(HttpContext context, IAuthenticationService authenticationService)
        {
            try
            {
                await authenticationService.ValidateApiRequestConditions(context.Request.Headers);
                await _next(context);
            }
            catch (InvalidCredentialException ex)
            {
                _logger.LogWarning((int)LogEvents.InvalidCredential, ex, "{0} | {1} | {2}", DateTime.Now, LogEvents.InvalidCredential, context.Request.Headers);
                context.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                context.Response.Headers.Add(AuthenticateHeader, _options.Value.AuthenticationMethods);
            }
            catch (NotSupportedException ex)
            {
                _logger.LogWarning((int)LogEvents.NoRequiredHeader, ex, "{0} | {1} | {2}", DateTime.Now, LogEvents.NoRequiredHeader, context.Request.Headers);
                context.Response.StatusCode = (int) HttpStatusCode.NotAcceptable;

                if (!context.Request.Headers.ContainsKey(AcceptHeader))
                    context.Response.Headers.Add(AcceptHeader, _options.Value.AcceptableHeaders );
            }
        }


        private const string AcceptHeader = "Accept";
        private const string AuthenticateHeader = "WWW-Authenticate";

        private readonly ILogger<AuthenticationHandler> _logger;
        private readonly RequestDelegate _next;
        private readonly IOptions<AuthenticationOptions> _options;
    }
}
