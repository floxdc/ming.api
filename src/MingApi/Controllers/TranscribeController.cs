﻿using System;
using System.Threading.Tasks;
using Floxdc.Ming.Core.BaseTypes;
using Floxdc.MingApi.Infrastructure;
using Floxdc.MingApi.Models;
using Floxdc.MingApi.Models.Dictionary;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Floxdc.MingApi.Controllers
{
    [Route("api/[controller]")]
    public class TranscribeController : ControllerBase
    {
        public TranscribeController(IMemoryCache cache, ILogger<TranscribeController> logger, DictionaryService service)
        {
            _cache = cache;
            _logger = logger;
            _service = service;
        }


        [HttpGet("{origin}/{inputLang}/{outputLang}/{description}/{source}")]
        public async Task<IActionResult> Get(string origin, string inputLang, string outputLang, string description, string source)
        {
            var fullTag = GetTag(origin, inputLang, outputLang, description, source);

            IActionResult response = null;
            string transcribed;
            if (_cache.TryGetValue(fullTag, out transcribed))
                response = Content(transcribed, "application/json");

            if (response == null)
                response = await GetResponse(origin, inputLang, outputLang, description, source).ConfigureAwait(false);

            _logger.LogInformation((int)LogEvents.TranscriptionRequest, null, "{0} | {1} | {2} | {3}", DateTime.Now, LogEvents.TranscriptionRequest, fullTag, response);
            return response;
        }


        private async Task<DictionaryContainer> GetContainer(IDictionaryInfo dictionaryInfo)
        {
            var shortTag = GetTag(dictionaryInfo.Origin, dictionaryInfo.Input, dictionaryInfo.Output, dictionaryInfo.Description);
            DictionaryContainer container;

            if (_cache.TryGetValue(shortTag, out container))
                return container;

            container = await _service.GetContainer(dictionaryInfo);
            if (container != null)
                _cache.Set(shortTag, container, CacheOptions.DayLong);

            return container;
        }


        private DictionaryInfoSlim GetDictionaryInfo(string origin, string inputLang, string outputLang, string description)
        {
            if (string.IsNullOrWhiteSpace(origin))
                return null;

            if (string.IsNullOrWhiteSpace(inputLang))
                return null;

            if (string.IsNullOrWhiteSpace(outputLang))
                return null;

            return DictionaryInfoSlim.Create(origin, inputLang, outputLang, description);
        }


        private async Task<IActionResult> GetResponse(string origin, string inputLang, string outputLang, string description, string source)
        {
            var fullTag = GetTag(origin, inputLang, outputLang, description, source);

            var dictionaryInfo = GetDictionaryInfo(origin, inputLang, outputLang, description);
            if (dictionaryInfo == null)
            {
                _logger.LogWarning((int)LogEvents.UnprocessableEntity, null, "{0} | {1} | {2}", DateTime.Now,
                    LogEvents.IoErrorCantGetDictionary, fullTag);
                return StatusCode(422);
            }

            var transcription = await GetTranscription(dictionaryInfo, source);
            var transcribed = JsonConvert.SerializeObject(transcription);
            _cache.Set(fullTag, transcribed, CacheOptions.Default);

            return Content(transcribed, "application/json");
        }


        private async Task<TranscriptionResult> GetTranscription(IDictionaryInfo dictionaryInfo, string source)
        {
            var container = await GetContainer(dictionaryInfo);
            if (container == null)
            {
                _logger.LogWarning((int) LogEvents.IoErrorCantGetContainer, null, "{0} | {1} | {2}", DateTime.Now,
                    LogEvents.IoErrorCantGetContainer, dictionaryInfo);
                return new TranscriptionResult(isTranscribed: false, content: string.Empty);
            }

            //TODO: replace hard-coded argument
            var transcriber = _service.GetTranscriber(container, isVerbose: true);

            var transcribed = source;
            var isTranscribed = transcriber?.TryAuto(source, out transcribed);

            return new TranscriptionResult(isTranscribed != null && isTranscribed.Value, transcribed);
        }


        private string GetTag(string origin, string inputLang, string outputLang, string description, string source = null)
            => $"{origin}/{inputLang}/{outputLang}/{description}/{source}";


        private readonly IMemoryCache _cache;
        private readonly ILogger<TranscribeController> _logger;
        private readonly DictionaryService _service;
    }
}