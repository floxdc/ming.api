﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Floxdc.MingApi.Infrastructure;
using Floxdc.MingApi.Models.Dictionary;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Floxdc.MingApi.Controllers
{
    [Route("api/[controller]")]
    public class DictionariesController : ControllerBase
    {
        public DictionariesController(IMemoryCache cache, ILogger<DictionariesController> logger, DictionaryService service)
        {
            _cache = cache;
            _logger = logger;
            _service = service;
        }


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            const string listTag = "DictionaryCodex";

            IActionResult response = null;
            string dictionaries;
            if (_cache.TryGetValue(listTag, out dictionaries))
                response = Content(dictionaries, "application/json");

            if (response == null)
                response = await GetResponse(listTag);

            _logger.LogInformation((int)LogEvents.DictionaryRequest, null, "{0} | {1} | {2}", DateTime.Now,
                LogEvents.DictionaryRequest, response);
            return response;
        }


        private async Task<IActionResult> GetResponse(string tag)
        {
            var codex = await _service.GetCodex().ConfigureAwait(false);
            if (!codex.Any())
            {
                _logger.LogError((int)LogEvents.IoErrorCantGetDictionary, null, "{0} | {1}", DateTime.Now,
                    LogEvents.IoErrorCantGetDictionary);
                return StatusCode(503);
            }

            var dictionaries = JsonConvert.SerializeObject(codex);
            _cache.Set(tag, dictionaries, CacheOptions.Default);

            return Content(dictionaries, "application/json");
        }


        private readonly IMemoryCache _cache;
        private readonly ILogger<DictionariesController> _logger;
        private readonly DictionaryService _service;
    }
}
