﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace Floxdc.MingApi.Infrastructure
{
    internal static class CacheOptions
    {
        internal static MemoryCacheEntryOptions Default
            => new MemoryCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromDays(1),
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(4)
            };

        internal static MemoryCacheEntryOptions DayLong
            => new MemoryCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromDays(1),
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1)
            };
    }
}
