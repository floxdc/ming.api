﻿namespace Floxdc.MingApi.Infrastructure
{
    public enum LogEvents
    {
        ApplicationRoleCantBeSet = 100,
        DeserializationError = 150,
        IoErrorCantGetContainer = 190,
        IoErrorCantGetDictionary = 110,
        DictionaryRequest = 210,
        TranscriptionRequest = 250,
        UnprocessableEntity = 290,
        InvalidCredential = 310,
        NoRequiredHeader = 350
    }
}
