﻿using Microsoft.Extensions.Primitives;

namespace Floxdc.MingApi.Infrastructure
{
    public class AuthenticationOptions
    {
        public StringValues AcceptableHeaders { get; set; } = new [] { "application/vnd.floxdc.ming.api-v1+json" };
        
        public StringValues AuthenticationMethods { get; set; } = new[] {"Basic"};
    }
}
