﻿namespace Floxdc.MingApi.Infrastructure
{
    public class SqlOptions
    {
        public string ApplicationRole { get; set; } = "mingapi";
        public string ApplicationRolePass { get; set; }
        public int SqlDelayTimeout { get; set; } = 5;
        public string SqlServerName { get; set; } = "Nagoya";
    }
}
