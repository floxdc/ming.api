﻿namespace Floxdc.MingApi.Infrastructure
{
    public class DriveOptions
    {
        public string DataFolder { get; set; }
        public string DictionaryFolder { get; set; }
        public string WebRootFolder { get; set; }
    }
}
