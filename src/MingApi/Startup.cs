﻿using System;
using System.Collections.Generic;
using Floxdc.MingApi.Infrastructure;
using Floxdc.MingApi.Middleware;
using Floxdc.MingApi.Models;
using Floxdc.MingApi.Models.Db;
using Floxdc.MingApi.Models.Dictionary;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Floxdc.MingApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: true, reloadOnChange: true)
                .AddInMemoryCollection(new Dictionary<string, string>
                {
                    {"webRootPath", env.WebRootPath}
                });
                
            if(env.IsDevelopment())
                builder.AddUserSecrets();

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<AuthenticationOptions>(o =>
            {
                o.AcceptableHeaders = Configuration["AuthenticationOptions:acceptableHeaders"];
                o.AuthenticationMethods = Configuration["AuthenticationOptions:authenticationMethods"];
            });
            services.Configure<DriveOptions>(o =>
            {
                o.DataFolder = Configuration["DriveOptions:dataFolder"];
                o.DictionaryFolder = Configuration["DriveOptions:dictionaryFolder"];
                o.WebRootFolder = Configuration["webRootPath"];
            });
            services.Configure<SqlOptions>(o =>
            {
                o.ApplicationRole = Configuration["SqlOptions:applicationRole"];
                o.ApplicationRolePass = Configuration["appRolePass"];
                o.SqlDelayTimeout = Convert.ToInt32(Configuration["SqlOptions:sqlDelayTimeout"]);
                o.SqlServerName = Configuration["SqlOptions:sqlServerName"];
            });

            services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<IContainerManager, ContainerManagerProxy>();
            services.AddScoped<DictionaryService, DictionaryService>();
            services.AddTransient<IRepository, SqlRepository>();

            services.AddDbContext<MingdbContext>(
                options => options.UseSqlServer(
                    GetConnectionString(),
                    builder => builder.CommandTimeout(int.Parse(Configuration["SqlOptions:sqlDelayTimeout"])))
                );

            services.AddMvc();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseAuthenticationHandler();
            app.UseMvc();

            app.Run(async context =>
            {
                await context.Response.WriteAsync("Use /api/ to reach services!");
            });
        }


        private string GetConnectionString(string name = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = Configuration["SqlOptions:sqlServerName"];

            var key = Configuration["dbLogin"];
            var pass = Configuration["dbPass"];
            return string.Format(Configuration.GetConnectionString(name), key, pass);
        }


        public static IConfigurationRoot Configuration { get; set; }
    }
}
