﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Floxdc.MingApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseIISIntegration()
                .UseContentRoot(Directory.GetCurrentDirectory())
#if DEBUG 
                .UseUrls("http://localhost:54959")
#endif
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
