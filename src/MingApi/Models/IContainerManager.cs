﻿using Floxdc.Ming.Core.BaseTypes;

namespace Floxdc.MingApi.Models
{
    public interface IContainerManager
    {
        DictionaryContainer Deserialize(string path);
        DictionaryContainer DeserializeFromString(string json);
        void Serialize(string path, DictionaryContainer container);
        string SerializeToString(DictionaryContainer container);
    }
}
