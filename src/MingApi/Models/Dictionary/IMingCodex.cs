﻿using System.Collections.Generic;

namespace Floxdc.MingApi.Models.Dictionary
{
    public interface IMingCodex
    {
        string Language { get; }
        IEnumerable<IDictionaryInfo> Dictionaries { get; }
    }
}