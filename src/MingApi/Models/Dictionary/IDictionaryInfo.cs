﻿namespace Floxdc.MingApi.Models.Dictionary
{
    public interface IDictionaryInfo
    {
        string Description { get; }
        string Input { get; }
        string Output { get; }
        string Origin { get; }
    }
}
