﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Floxdc.Ming.Core.BaseTypes;
using Microsoft.FSharp.Core;

namespace Floxdc.MingApi.Models.Dictionary
{
    public class DictionaryService
    {
        public DictionaryService(IRepository repository)
        {
            _repository = repository;
        }


        internal async Task<DictionaryContainer> GetContainer(IDictionaryInfo dictionaryInfo) 
            => await _repository.GetDictionaryContainer(dictionaryInfo);


        internal async Task<IEnumerable<IMingCodex>> GetCodex()
        {
            var dictionaryData = await _repository.GetListOfDictionaries();
            var dictionaryGroups = dictionaryData.GroupBy(d => d.Origin);

            return (dictionaryGroups
                .Select(dictionaryGroup => new MingCodex
                {
                    Language = dictionaryGroup.Key,
                    Dictionaries = dictionaryGroup
                        .Select(d => DictionaryInfoSlim.Create(d.Origin, d.Input, d.Output, d.Description))
                }))
                .Cast<IMingCodex>()
                .AsEnumerable();
        }


        internal Ming.Control.Ming GetTranscriber(DictionaryContainer container, bool isVerbose)
            => container != null
                ? new Ming.Control.Ming(container, new FSharpOption<bool>(isVerbose))
                : null;


        private readonly IRepository _repository;
    }
}
