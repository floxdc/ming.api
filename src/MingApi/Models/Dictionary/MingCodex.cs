﻿using System.Collections.Generic;

namespace Floxdc.MingApi.Models.Dictionary
{
    public class MingCodex : IMingCodex
    {
        public string Language { get; set; }
        public IEnumerable<IDictionaryInfo> Dictionaries { get; set; }
    }
}
