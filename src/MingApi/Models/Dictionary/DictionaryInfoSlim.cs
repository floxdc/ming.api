﻿namespace Floxdc.MingApi.Models.Dictionary
{
    public class DictionaryInfoSlim : IDictionaryInfo
    {
        private DictionaryInfoSlim(string origin, string inputLang, string outputLang, string description)
        {
            Input = inputLang;
            Origin = origin;
            Output = outputLang;
            Description = description ?? string.Empty;
        }


        internal static DictionaryInfoSlim Create(string origin, string inputLang, string outputLang, string description) 
            => new DictionaryInfoSlim(origin, inputLang, outputLang, description);


        public string Description { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public string Origin { get; set; }
    }
}
