﻿namespace Floxdc.MingApi.Models
{
    public class TranscriptionResult
    {
        internal TranscriptionResult(bool? isTranscribed, string content)
        {
            IsTranscribed = isTranscribed ?? false;
            Content = content;
        }


        public bool IsTranscribed { get; set; }
        public string Content { get; }
    }
}
