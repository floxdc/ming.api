﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Floxdc.Ming.Core.BaseTypes;
using Floxdc.MingApi.Infrastructure;
using Floxdc.MingApi.Models.Dictionary;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Floxdc.MingApi.Models.Dirive
{
    internal sealed class Drive : IRepository
    {
        public Drive(IContainerManager manager, ILogger<Drive> logger, IOptions<DriveOptions> options)
        {
            _manager = manager;
            _logger = logger;
            _options = options;
        }


        public Task<IEnumerable<IDictionaryInfo>> GetListOfDictionaries()
        {
            var scanPath = GetDirectoryPath;

            var dictionaryList = Directory.Exists(scanPath)
                ? GetDirectoryContent(scanPath)
                : GetDefaultDictionary(scanPath);

            var dictionaryData = new List<IDictionaryInfo>();

            foreach (var dictionary in dictionaryList)
            {
                var container = GetContainer(dictionary);

                if (container == null)
                    continue;

                var data = GetInnerDictionaryData(container);
                dictionaryData.Add(data);
            }

            return Task.FromResult(dictionaryData.AsEnumerable());
        }


        public Task<DictionaryContainer> GetDictionaryContainer(IDictionaryInfo info)
        {
            var name = $"{info.Origin}_{info.Input}_{info.Output}";
            if (!string.IsNullOrWhiteSpace(info.Description))
                name = $"{name}+{info.Description}";

            var path = GetContainerPath(name);
            if (!File.Exists(path))
                _logger.LogWarning((int) LogEvents.IoErrorCantGetContainer, null, "{0} | {1} | {2}", DateTime.Now,
                    LogEvents.IoErrorCantGetContainer, path);

            var container = GetContainer(path);
            return Task.FromResult(container);
        }


        private DictionaryContainer GetContainer(string path)
        {
            DictionaryContainer container = null;

            try
            {
                container = _manager.Deserialize(path);
            }
            catch (Exception ex)
            {
                _logger.LogError((int) LogEvents.DeserializationError, ex, "{0} | {1} | {2}", DateTime.Now,
                    LogEvents.DeserializationError, ex.Message);
            }

            return container;
        }


        private string GetContainerPath(string name)
        {
            var path = Path.Combine(GetDirectoryPath, name);
            return Path.ChangeExtension(path, "dic");
        }


        private IEnumerable<string> GetDefaultDictionary(string path)
        {
            _logger.LogWarning((int) LogEvents.IoErrorCantGetDictionary, null, "{0} | {1} | {2}", DateTime.Now,
                LogEvents.DeserializationError, path);
            return new List<string>().AsEnumerable();
        }


        private IEnumerable<string> GetDirectoryContent(string path)
        {
            IEnumerable<string> results = new List<string>();

            try
            {
                results = Directory
                    .GetFiles(path)
                    .Select(Path.GetFileName);
            }
            catch (Exception ex)
            {
                _logger.LogError((int) LogEvents.IoErrorCantGetDictionary, ex, "{0} | {1} | {2}", DateTime.Now,
                    LogEvents.IoErrorCantGetDictionary, ex.Message);
            }

            return results;
        }


        //TODO: handle empty config exceptions
        private string GetDirectoryPath
            => Path.Combine(_options.Value.WebRootFolder, _options.Value.DataFolder, _options.Value.DictionaryFolder);


        private IDictionaryInfo GetInnerDictionaryData(DictionaryContainer container)
            => DictionaryInfoSlim.Create(container.OriginCulture.Name, container.InputCulture.Name,
                container.OutputCulture.Name, container.Description);


        private readonly ILogger _logger;
        private readonly IOptions<DriveOptions> _options;
        private readonly IContainerManager _manager;
    }
}