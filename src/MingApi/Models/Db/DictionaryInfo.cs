﻿using System;

namespace Floxdc.MingApi.Models.Db
{
    public partial class DictionaryInfo
    {
        public int Id { get; set; }
        public string Origin { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public virtual DictionaryContent DictionaryContent { get; set; }
    }
}
