﻿using System;

namespace Floxdc.MingApi.Models.Db
{
    public partial class BotPreferences
    {
        public string UserId { get; set; }
        public string Content { get; set; }
        public bool IsActive { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
