﻿using System;

namespace Floxdc.MingApi.Models.Db
{
    public partial class DictionaryContent
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public virtual DictionaryInfo IdNavigation { get; set; }
    }
}
