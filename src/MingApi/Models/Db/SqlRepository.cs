﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Floxdc.Ming.Core.BaseTypes;
using Floxdc.MingApi.Infrastructure;
using Floxdc.MingApi.Models.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Floxdc.MingApi.Models.Db
{
    public class SqlRepository : IRepository
    {
        public SqlRepository(MingdbContext context, IContainerManager manager, ILogger<SqlRepository> logger, IOptions<SqlOptions> options)
        {
            _context = context;
            _logger = logger;
            _manager = manager;
            _options = options;
        }


        public Task<IEnumerable<IDictionaryInfo>> GetListOfDictionaries()
        {
            SetApplicationRole();

            var dictionaries = _context
                .DictionaryInfo.Where(i => i.IsActive)
                .Cast<IDictionaryInfo>()
                .AsEnumerable();

            return Task.FromResult(dictionaries);
        }


        public Task<DictionaryContainer> GetDictionaryContainer(IDictionaryInfo info)
        {
            SetApplicationRole();

            var json = (from content in _context.DictionaryContent
                let dictionaryId = from i in _context.DictionaryInfo
                    where i.IsActive
                          && i.Origin == info.Origin
                          && i.Input == info.Input
                          && i.Output == info.Output
                          && i.Description == info.Description
                    select i.Id
                where content.Id == dictionaryId.FirstOrDefault()
                select content.Content).FirstOrDefault();

            var result = GetContainer(json);
            return Task.FromResult(result);
        }


        private DictionaryContainer GetContainer(string json)
        {
            DictionaryContainer container = null;

            try
            {
                container = _manager.DeserializeFromString(json);
            }
            catch (Exception ex)
            {
                _logger.LogError((int) LogEvents.DeserializationError, ex, "{0} | {1} | {2}", DateTime.Now,
                    LogEvents.DeserializationError, ex.Message);
            }

            return container;
        }


        private void SetApplicationRole()
        {
            try
            {
                _context.Database.OpenConnection();
                _context.Database.ExecuteSqlCommand(
                    $"exec sp_setapprole '{_options.Value.ApplicationRole}', '{_options.Value.ApplicationRolePass}'");
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LogEvents.ApplicationRoleCantBeSet, ex, "{0} | {1} | {2}", DateTime.Now,
                    LogEvents.ApplicationRoleCantBeSet, ex.Message);
            }
        }


        private readonly MingdbContext _context;
        private readonly ILogger _logger;
        private readonly IContainerManager _manager;
        private readonly IOptions<SqlOptions> _options;
    }
}
