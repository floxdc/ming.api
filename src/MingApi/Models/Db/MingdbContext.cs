﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Floxdc.MingApi.Models.Db
{
    public sealed partial class MingdbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DictionaryContent>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Updated).HasColumnType("datetime");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.DictionaryContent)
                    .HasForeignKey<DictionaryContent>(d => d.Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_DictionaryContent_DictionaryInfo");
            });

            modelBuilder.Entity<DictionaryInfo>(entity =>
            {
                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Input)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.Origin)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.Output)
                    .IsRequired()
                    .HasMaxLength(8);

                entity.Property(e => e.Updated).HasColumnType("datetime");
            });
        }

        public DbSet<DictionaryContent> DictionaryContent { get; set; }
        public DbSet<DictionaryInfo> DictionaryInfo { get; set; }
    }
}