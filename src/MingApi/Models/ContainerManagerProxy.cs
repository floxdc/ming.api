﻿using System;
using Floxdc.Ming.ContainerManager;
using Floxdc.Ming.Core.BaseTypes;
using Nessos.FsPickler.Json;

namespace Floxdc.MingApi.Models
{
    internal class ContainerManagerProxy : IContainerManager
    {
        public ContainerManagerProxy()
        {
            //_manager = new ContainerManager(); 
            _manager = Nessos.FsPickler.Json.FsPickler.CreateJsonSerializer();
        }


        public DictionaryContainer Deserialize(string path)
        {
            throw new NotImplementedException(); //return _manager.Deserialize(path); 
        }


        //public DictionaryContainer DeserializeFromString(string json)
            //=> _manager.DeserializeFromString(json); // UnPickleOfString<DictionaryContainer>(json);


        public DictionaryContainer DeserializeFromString(string json)
            => _manager.UnPickleOfString<DictionaryContainer>(json);


        public void Serialize(string path, DictionaryContainer container)
        {
            throw new NotImplementedException(); //_manager.Serialize(path, container);
        }


        public string SerializeToString(DictionaryContainer container)
        {
            throw new NotImplementedException(); // _manager.SerializeToString(container);
        }


        //private readonly ContainerManager _manager;
        private readonly JsonSerializer _manager;
    }
}
