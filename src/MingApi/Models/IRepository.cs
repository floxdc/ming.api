﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Floxdc.Ming.Core.BaseTypes;
using Floxdc.MingApi.Models.Dictionary;

namespace Floxdc.MingApi.Models
{
    public interface IRepository
    {
        Task<IEnumerable<IDictionaryInfo>> GetListOfDictionaries();
        Task<DictionaryContainer> GetDictionaryContainer(IDictionaryInfo info);
    }
}
